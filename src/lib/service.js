async function getData(userid) {
  let response = await axios.get(`https://jsonplaceholder.typicode.com/users/${userid}`);
  let data = await axios.get(`https://jsonplaceholder.typicode.com/posts?userId=${userid}`);
  console.log(response.data);
  console.log(data.data);
  return response+data;
}
export default getData;